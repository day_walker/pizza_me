const request = require('superagent')
const baseURL = 'localhost:3000/groups_groups'
const group_groups = require('.../test-data/user_groups')
describe('Backend API', function() {
  beforeAll((done)=>{
      request.post(baseURL)
      .set('Accept', 'application/json')
      .send(group_groups)
      .end(function(error, response){
          expect(response.status).toBe(201)
      })
  });
  it ('should return the list of groups', function (done) {
   request(baseURL, function (error, response){
          expect(response.status).toBe(200)
          expect(response.body[0].group).toBe('admin')
          done()
      })
  })
  it('should insert one group', (done) => {
      request.post(baseURL+'')
      .set('Accept', 'application/json')
      .send({group: 'john Does group'})
      .end(function (error, response){
            expect(response.status).toBe(201)
         done()   
      })
       request.get(baseURL)
       .end(function(error, response){
           expect(response.status).toBe(200)
           expect(response.body.slice(-1).pop().group).toBe('john Does group')
          done() 
       })
       
   })
 
  it('should delete one group by id', (done) => {
      request.post(baseURL)
       .set('Accept', 'application/json')
        .type('json')
        .send({id: 200, groupname: 'kui Mwakesi', branch_id : '24'})
        .end((error, response)=>{
            response.body
            done()
         })
          
      request.delete(`${baseURL}/200`)
         .end((error, response) => {
         expect(response.status).toBe(200)
            done()
       })
}) 

        
});
describe('for individual records', ()=>{
      it('should get one group', (done) =>{
      request.get(baseURL+'/2')
       .end(function(error, response){
           expect(response.status).toBe(200)
           expect(response.body.groupname).toBe('Sales')
           expect(response.body.id).toBe(2)
          done() 
       })
       
  })  
  it('should update one group', (done) =>{
   afterAll((done) => {
    request.patch(baseURL+'/2')
     .set('Accept', 'application/json')
      .type('json')
      .send({groupname: 'Sales and Marketing'})
       .end(function(error, response){
           expect(response.status).toBe(200)      
       })
       done()
  })
      request.patch(baseURL+'/2')
      .set('Accept', 'application/json')
      .type('json')
      .send({groupname: 'Sales'})
       .end(function(error, response){
           expect(response.status).toBe(200) 
           done()     
       })
       
   }) 
      
  
});