const request = require ('superagent');
const mime = require('mime-types');
var baseUrl = "localhost:3000/pizzas";


describe('Pizzas API', () => {
    describe('Given the root URL', () => {
        beforeAll(done => {
            //Post a record to ensure there will always be one record.
            request.post(baseUrl)
                .send({
                    "id":2000,
                    "price_id": 2000, 
                    "description": "this is how a pizza is described" ,
                    "image": 'image.jpg'
                }) 
                .type('json')
                .end((err, res) => {
                    expect(res.status).toBe(201);
                    request.get(`${baseUrl}/2000` , (error, response) => {
                        var entry = response.body;
                        expect(response.status).toBe(200);
                        expect(entry.id).toBe(2000);
                        expect(entry.price_id).toBe(2000);
                        expect(entry.description).toBe('this is how a pizza is described');
                        expect(entry.image).toBe('image.jpg');
                        expect(mime.lookup(entry.image)).toBe('image/jpeg');
                        done();
                    });
                });
        });
        afterAll(done => {
            //Delete record after testing
            request.delete(`${baseUrl}/2000`)
            .end((err, res) => {
                expect(res.status).toBe(200);
                done();
            });
        });
    
        it('should GET a list of all pizzas', (done) => {
            request.get(baseUrl, (error, response) => {
                expect(response.status).toBe(200);
                expect(response.body[0].id).not.toBeNull();
                done();
            });
        });

        it('should POST one pizza', (done) => {
            request.post(baseUrl)
            .send({
                "id" : 40000,
                "price_id" : 40000,
                "description" : "a short description",
                "image" : 'image.gif'
            })
            .type('json')
            .end((err, res) => {
                expect(res.status).toBe(201);
                request.get(`${baseUrl}/40000`, (error, response) => {
                    var entry = response.body;
                    expect(response.status).toBe(200);
                    expect(entry.id).toBe(40000);
                    expect(entry.price_id).toBe(40000);
                    expect(entry.description).toBe("a short description");
                    expect(entry.image).toBe('image.gif');
                    expect(mime.lookup(entry.image)).toBe('image/gif');
                    done();
                });        
            });
         });
    });

        describe('Given a pizza id' , () => {  
            beforeAll((done) => {
                //test for Delete
                request.post(baseUrl)
                .send({
                    "id" : 350,
                    "price_id" : 30,
                    "description" : "another description", 
                    "image" : "image.png"

                })
                .type('json')
                .end((err,res) => {
                    expect(res.status).toBe(201);
                    request.get(`${baseUrl}/350`)
                        .end((err, res) => {
                            expect(res.status).toBe(200)
                            done()
                        });
                });
                //test for patch/update
                request.post(baseUrl)
                .send({
                    "id" : 5000,
                    "price_id" : 5000,
                    "description" : "my description",
                    "image" : "image.png"
                })
                .type('json')
                .end((err,res) => {
                    expect(res.status).toBe(201);
                });
            });

            afterAll((done) => {
                //delete record in database
                request.delete(`${baseUrl}/5000`)
                .end((err,res) => {
                    expect(res.status).toBe(200);
                    done();
                });
            });

            it('should GET one pizza' , (done) => {   
                request.get(`${baseUrl}/350`, (error, response) =>{
                    
                        var ent = response.body;
                        expect(response.status).toBe(200);
                        expect(ent.id).toBe(350);
                        expect(ent.price_id).toBe(30);
                        expect(ent.description).toBe("another description");
                        expect(ent.image).toBe('image.png');
                        expect(mime.lookup(ent.image)).toBe('image/png');
                        done();                
                });
            });

            it('should PATCH/UPDATE one pizza', (done) => {
                request.patch(`${baseUrl}/5000`)
                .send({
                    "description" : "patched description",
                    "image" : 'image.png'
                })
                .end((error,response) => {
                    var ent = response.body;
                    expect(response.status).toBe(200);
                    expect(ent.id).toBe(5000);
                    expect(ent.price_id).toBe(5000);
                    expect(ent.description).toBe("patched description");
                    expect(ent.image).toBe('image.png');
                    expect(mime.lookup(ent.image)).toBe('image/png');
                    done();
                });
            });

            it('should DELETE one pizza', (done) => {
                request.delete(`${baseUrl}/350`, (error,response) => {
                    expect(response.status).toBe(200);
                    done();
                });
                request.get(`${baseUrl}/350`, (error,response) => {
                    expect(response.status).toBe(404);
                    done();
                });
            });
        });
    });
