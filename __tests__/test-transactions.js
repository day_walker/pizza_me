const request = require('superagent')
const baseURL = 'localhost:3000/transactions'


describe ("Transactions API", () => {
    it (`should GET all Transactions when url: ${baseURL} is called.`, (done) => {
        request.get(`${baseURL}`)
            .end((err, res) => {
                expect(res.status).toBe(200)
                done()
            })
    })
    it (`should POST one transaction when the url:${baseURL} is called`, (done) => {
        var _id = 3000000
        request.post(`${baseURL}`)
            .set('Accept', 'application/json')
            .send({id:_id, name: 'john Doe'}) // TO DO: Change this!!!!
            .end((err, res) => {
                expect(res.status).toBe(201)
                request.get(`${baseURL}/${_id}`)
                    .end((err, res) => {
                        expect(res.status).toBe(200)
                        done()
                    })
            })
    })
})