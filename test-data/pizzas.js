var faker = require('faker')

function generatePizzas () {
  var pizzas = []
  //pizza_id,price_id,description,image

  for (var id = 0; id < 50; id++) {
    var price_id = id; 
    var description = faker.lorem.paragraph();
    var image = faker.image.imageUrl();
    
    pizzas.push({
      "id": id,
      "price_id": price_id,
      "description": description,
      "image": image

    })
  }

  return { "pizzas": pizzas }
}

// json-server requires that you export
// a function which generates the data set
module.exports = generatePizzas;
